const mongoose = require('mongoose');
const Joi = require('joi');

const quickReplyInfoSchema = new mongoose.Schema({
    content_type: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    payload: {
        type: String,
        required: false,
        unique: false
    },
    nextMessageId: {
        type: String,
        required: false
    },
    nextMessageType: {
        type: String,
        required: false
    }
});

const QuickReplyInfo = mongoose.model('QucikReplyInfo', quickReplyInfoSchema);

const assignPayload = (quickReply) => {
    quickReply.payload = Date.now().toString();
    return quickReply;
};

const validateQuickReplyInfo = (quickReplyInfo) => {
    const schema = {
        content_type: Joi.string().required(),
        title: Joi.string().required(),
        payload: Joi.string(),
        nextMessageId: Joi.objectId(),
        nextMessageType: Joi.any().allow('image', 'gif', 'audio', 'video', 'file', 'button', 'generic', 'receipt', 'quick reply', 'read receipt', 'typing on', 'typing off', 'account linking', 'flow')
    };
    return Joi.validate(quickReplyInfo, schema);
};

exports.quickReplySchema = quickReplyInfoSchema;
exports.QuickReplyInfo = QuickReplyInfo;
exports.validateQuickReplyInfo = validateQuickReplyInfo;
exports.assignPayload = assignPayload;