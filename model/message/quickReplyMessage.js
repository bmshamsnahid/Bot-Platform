const mongoose = require('mongoose');
const Joi = require('joi');

const quickReplyMessageSchema = new mongoose.Schema({
    triggerKeyword: {
        type: [String],
        required: true
    },
    flowId: {
        type: String,
        required: true
    },
    text: {
        type: String,
        required: true
    },
    quick_replies: {
        type: [String],
        required: true
    }
});

const QuickReplyMessage = mongoose.model('QuickReplyMessage', quickReplyMessageSchema);

const validateQuickReplyMessage = (quickReplyMessage) => {
    const schema = {
        triggerKeyword: Joi.array().min(1).items(Joi.string()),
        flowId: Joi.objectId().required(),
        text: Joi.string().required(),
        quick_replies: Joi.array().items(Joi.objectId())
    };
    return Joi.validate(quickReplyMessage, schema);
};

exports.quickReplyMessageSchema = quickReplyMessageSchema;
exports.QuickReplyMessage = QuickReplyMessage;
exports.validateQuickReplyMessage = validateQuickReplyMessage;