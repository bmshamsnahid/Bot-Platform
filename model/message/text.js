const Joi = require('joi');
const mongoose = require('mongoose');

const textMessageSchema = new mongoose.Schema({
    text: {
        type: String,
        required: true,
        minLength: 1,
        maxLength: 2000
    },
    metadata: {
        type: String,
        required: false,
        default: "Softograph Bot",
        maxLength: 1000
    }
});

const textMessage = mongoose.model('TextMessage', textMessageSchema);

const validateTextMessage = (textMessage) => {
    const schema = {
        text: Joi.string().min(1).max(2000).required(),
        metadata: Joi.string().max(1000)
    };
    return Joi.validate(textMessage, schema);
};

exports.textMessageSchema = textMessageSchema;
exports.TextMessage = textMessage;
exports.validateTextMessage = validateTextMessage;
