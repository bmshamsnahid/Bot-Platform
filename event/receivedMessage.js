const anchorMessageLib = require('../messageSend/anchor');
const sendTextMessageLib = require('../messageSend/text');
const sendImageMessageLib = require('../messageSend/image');
const sendGifMessageLib = require('../messageSend/gif');
const sendAudioMessageLib = require('../messageSend/audio');
const sendVideoMessageLib = require('../messageSend/video');
const sendFileMessageLib = require('../messageSend/file');
const sendButtonMessageLib = require('../messageSend/button');
const sendGenericMessageLib = require('../messageSend/generic');
const sendReceiptMessageLib = require('../messageSend/receipt');
const sendQuickReplyMessageLib = require('../messageSend/quickReply');
const sendReadReceiptMessageLib = require('../messageSend/readReceipt');
const sendTypingOnMessageLib = require('../messageSend/typingOn');
const sendTypingOffMessageLib = require('../messageSend/typingOff');
const sendAccountLinkingMessageLib = require('../messageSend/accountLinking');

const quickReplyHandler = require('../controller/handlePayload');

/*
 * Message Event
 *
 * This event is called when a message is sent to your page. The 'message'
 * object format can vary depending on the kind of message that was received.
 * Read more at https://developers.facebook.com/docs/messenger-platform/webhook-reference/message-received
 *
 * For this example, we're going to echo any text that we get. If we get some
 * special keywords ('button', 'generic', 'receipt'), then we'll send back
 * examples of those bubbles to illustrate the special message bubbles we've
 * created. If we receive a message with an attachment (image, video, audio),
 * then we'll simply confirm that we've received the attachment.
 *
 */
module.exports = (event, botId) => {
    var senderID = event.sender.id;
    var recipientID = event.recipient.id;
    var timeOfMessage = event.timestamp;
    var message = event.message;

    console.log("Received message for user %d and page %d at %d with message:",
        senderID, recipientID, timeOfMessage);
    console.log(JSON.stringify(message));

    var isEcho = message.is_echo;
    var messageId = message.mid;
    var appId = message.app_id;
    var metadata = message.metadata;

    // You may get a text or attachment but not both
    var messageText = message.text;
    var messageAttachments = message.attachments;
    var quickReply = message.quick_reply;

    if (isEcho) {
        // Just logging message echoes to console
        console.log("Received echo for message %s and app %d with metadata %s",
            messageId, appId, metadata);
        return;
    } else if (quickReply) {
        var quickReplyPayload = quickReply.payload;
        console.log("Quick reply for message %s with payload %s",
            messageId, quickReplyPayload);

        quickReplyHandler.handleQuickReply(quickReply, senderID, recipientID);
        return;
    }

    if (messageText) {

        // If we receive a text message, check to see if it matches any special
        // keywords and send back the corresponding example. Otherwise, just echo
        // the text we received.
        switch (messageText) {
            case 'image':
                sendImageMessageLib(senderID);
                break;

            case 'gif':
                sendGifMessageLib(senderID);
                break;

            case 'audio':
                sendAudioMessageLib(senderID);
                break;

            case 'video':
                sendVideoMessageLib(senderID);
                break;

            case 'file':
                sendFileMessageLib(senderID);
                break;

            case 'button':
                sendButtonMessageLib(senderID);
                break;

            case 'generic':
                sendGenericMessageLib(senderID);
                break;

            case 'receipt':
                sendReceiptMessageLib(senderID);
                break;

            case 'quick reply':
                sendQuickReplyMessageLib(senderID);
                break;

            case 'read receipt':
                sendReadReceiptMessageLib(senderID);
                break;

            case 'typing on':
                sendTypingOnMessageLib(senderID);
                break;

            case 'typing off':
                sendTypingOffMessageLib(senderID);
                break;

            case 'account linking':
                sendAccountLinkingMessageLib(senderID);
                break;

            default:
                anchorMessageLib(senderID, messageText, recipientID);
        }
    } else if (messageAttachments) {
        sendTextMessageLib(senderID, messageText);
    }
}