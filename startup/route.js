const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const error = require('../middleware/error');

const webHookRouter = require('../router/webhook');
const authorizeRouter = require('../router/authorize');
const textMessageRouter = require('../router/message/textMessage');
const botInfoRouter = require('../router/bot/botInfo');
const flowInfoRouter = require('../router/flow/flowInfo');
const quickReplyMessageRouter = require('../router/message/quickReplyMessage');

const verifyRequestSignature = require('../lib/verifyRequestSignature');

module.exports = (app) => {
    app.use(express.json());
    app.use(bodyParser.json({ verify: verifyRequestSignature }));
    app.use(morgan('dev'));
    app.use('/webhook', webHookRouter);
    app.use('/authorize', authorizeRouter);
    app.use('/api/textMessage', textMessageRouter);
    app.use('/api/botInfo', botInfoRouter);
    app.use('/api/flowInfo', flowInfoRouter);
    app.use('/api/quickReplyMessage', quickReplyMessageRouter);
    app.use(error);
};