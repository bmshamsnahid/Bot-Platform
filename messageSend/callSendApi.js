const request = require('../helper/requestPromise');
const { BotInfo } = require('../model/bot/botInfo');

/*
 * Call the Send API. The message data goes in the body. If successful, we'll
 * get the message id in a response
 *
 */
module.exports = async (messageData, recipientID) => {
    const botInfo = await BotInfo.findOne({ pageId: recipientID });
    const options = {
        uri: 'https://graph.facebook.com/v2.6/me/messages',
        qs: { access_token: botInfo.pageAccessToken },
        method: 'POST',
        json: messageData
    };
    const res = await request(options);
    console.log('Message sending operation completed successfully.');
    console.log (res);
};