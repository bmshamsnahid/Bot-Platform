const callSendApi = require('./callSendApi');

/*
 * Turn typing indicator off
 *
 */
module.exports = (recipientId) => {
    console.log("Turning typing indicator off");

    const messageData = {
        recipient: {
            id: recipientId
        },
        sender_action: "typing_off"
    };

    callSendApi(messageData);
};