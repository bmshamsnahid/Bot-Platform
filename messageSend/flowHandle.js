const { FlowInfo } = require('../model/flow/flowInfo');
const quickReplyMessageLib = require('./quickReply');

module.exports = async (flowId, senderId, pageId) => {
    const flowInfo = await FlowInfo.findById(flowId);

    const nextMessageId = flowInfo.nextMessageId;
    const nextMessageType = flowInfo.nextMessageType;

    if (nextMessageType === 'quick reply') {
        quickReplyMessageLib(pageId, senderId, nextMessageId);
    }
};