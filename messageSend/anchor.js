const callSendApi = require('./callSendApi');
const { FlowInfo } = require('../model/flow/flowInfo');
const flowHandle = require('./flowHandle');
const textMessageLib = require('./text');
/*
 * Send a text message using the Send API.
 *
 */
module.exports = async (recipientId, messageText, pageId) => {
    const flowInfo = await FlowInfo.findOne({
        "triggerKeyword": { 
            $in: messageText
         }
    });

    if (flowInfo) {
        flowHandle(flowInfo._id, recipientId, pageId);
    } else {
        textMessageLib(recipientId, messageText, pageId);
    }
};
