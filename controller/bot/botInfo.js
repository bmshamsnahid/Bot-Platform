const { BotInfo, validateBotInfo, structureBotInfo } = require('../../model/bot/botInfo');
const _ = require('lodash');

const createBotInfo = async (req, res) => {
    const structuredBotInfo = structureBotInfo(req.body);

    const { error } = validateBotInfo(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const botInfo = new BotInfo(structuredBotInfo);
    const result = await botInfo.save();

    return res.status(200).json({
        success: true,
        data: result
    });
};

const getBotInfo = async (req, res, next) => {
    const botInfo = await BotInfo.findById(req.params.id);
    res.send(botInfo);
};

const getAllBotInfo = async (req, res, next) => {
    const botInfos = await BotInfo.find();
    res.send(botInfos);
};

const updateBotInfo = async (req, res, next) => {

};

const deleteBotInfo = async (req, res, next) => {

};

module.exports = {
  createBotInfo,
  getBotInfo,
  getAllBotInfo,
  updateBotInfo,
  deleteBotInfo
};