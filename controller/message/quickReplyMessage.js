const { QuickReplyMessage, validateQuickReplyMessage} = require('../../model/message/quickReplyMessage');
const quickReplyService = require('../../service/quickReply/quickReplyInfo');
const _ = require('lodash');

const createQuickReplyMessage = async (req, res, next) => {
    
    const quick_replies = req.body.quick_replies;
    const quickRepliesInfoResult = await quickReplyService.createMultipleQuickReplies(quick_replies);
    
    if (quickRepliesInfoResult.error) return res.status(400).send(error.details[0].message);
    
    const quickReplyMessageSkeleton = {
        triggerKeyword: req.body.triggerKeyword,
        flowId: req.body.flowId,
        text: req.body.text,
        quick_replies: quickRepliesInfoResult.quickRepliesId
    }

    const { error } = validateQuickReplyMessage(quickReplyMessageSkeleton);
    
    const quickReplyMessage = new QuickReplyMessage(quickReplyMessageSkeleton);
    const result = await quickReplyMessage.save();

    res.send(result);
};

const getQuickReplyMessage = async (req, res, next) => {
    const quickReplyMessage = await QuickReplyMessage.findById(req.params.id);
    res.send(quickReplyMessage);
};

const getQuickReplyMessages = async (req, res, next) => {
    const quickReplyMessages = await QuickReplyMessage.find();
    res.send(quickReplyMessages);
};

const updateQuickReplyMessage = async (req, res, next) => {

};

const deleteQuickReplyMessage = async (req, res, next) => {

};

module.exports = {
    createQuickReplyMessage,
    getQuickReplyMessage,
    getQuickReplyMessages,
    updateQuickReplyMessage,
    deleteQuickReplyMessage
};