const { TextMessage, validateTextMessage } = require('../../model/message/text');
const _ = require('lodash');

const createMessage = async (req, res, next) => {
    const { error } = validateTextMessage(req.body);
    if (error) {
        return res.status(400).send(error.details[0].message);
    }

    const textMessage = new TextMessage(_.pick(req.body, ['text', 'metadata']));
    const result = await textMessage.save();
    res.status(200).json({
        success: true,
        message: "Successfully created the plain message",
        data: result
    })
};

const getMessage = (req, res, next) => {

};

const getMessages = (req, res, next) => {

};

const updateMessage = (req, res, next) => {

};

const deleteMessage = () => {

};

module.exports = {
  createMessage,
  getMessage,
  getMessages,
  updateMessage,
  deleteMessage
};