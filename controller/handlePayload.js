const sendTextMessageLib = require('../messageSend/text');
const sendImageMessageLib = require('../messageSend/image');
const sendGifMessageLib = require('../messageSend/gif');
const sendAudioMessageLib = require('../messageSend/audio');
const sendVideoMessageLib = require('../messageSend/video');
const sendFileMessageLib = require('../messageSend/file');
const sendButtonMessageLib = require('../messageSend/button');
const sendGenericMessageLib = require('../messageSend/generic');
const sendReceiptMessageLib = require('../messageSend/receipt');
const sendQuickReplyMessageLib = require('../messageSend/quickReply');
const sendReadReceiptMessageLib = require('../messageSend/readReceipt');
const sendTypingOnMessageLib = require('../messageSend/typingOn');
const sendTypingOffMessageLib = require('../messageSend/typingOff');
const sendAccountLinkingMessageLib = require('../messageSend/accountLinking');
const callSendApi = require('../messageSend/callSendApi');

const { QuickReplyMessage } = require('../model/message/quickReplyMessage');
const { QuickReplyInfo } = require('../model/quickReply/quickReplyInfo');
const flowHandle = require('../messageSend/flowHandle');

let handleQuickReply = async (quickReply, senderId, pageId) => {
    const quickReplyPayload = quickReply.payload.split('-');
    
    // payload is consist of nextMessageType-nextMessageId
    const nextMessageType = quickReplyPayload[0];
    const nextMessageId = quickReplyPayload[1];

    if (nextMessageType === 'quick reply') {
        console.log('///////////////////////////////////');
        console.log('find out quick replly message');
        const quickReplyMessage = await QuickReplyMessage.findById(nextMessageId);
        const quickReplies = await getQuickReplies(quickReplyMessage.quick_replies);
        sendQuickReplyMessageLib(pageId, senderId, nextMessageId);
    } else if (nextMessageType === 'flow') {
        const flowId = nextMessageId;
        flowHandle(flowId, senderId, pageId);
    }
};

module.exports = {
    handleQuickReply
};

const getQuickReplies = async (quickRepliesId) => {
    const quickReplies = [];
    for (let index=0; index<quickRepliesId.length; index++) {
        const quickReplyInfo = await QuickReplyInfo.findById(quickRepliesId[index]).select('content_type title payload -_id');
        quickReplies.push(quickReplyInfo);
    }
    return quickReplies;
};