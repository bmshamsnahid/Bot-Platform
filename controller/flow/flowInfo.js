const { FlowInfo, validateFlowInfo, assignPayload } = require('../../model/flow/flowInfo');
const _ = require('lodash');

const createFlowInfo = async (req, res, next) => {
    
    const flowInfoObject = await assignPayload(_.pick(req.body, ['triggerKeyword', 'name', 'description', 'text', 'isActive', 'botsId', 'pagesId', 'ownerId']));
    const { error } = validateFlowInfo(flowInfoObject);
    if (error) return res.status(400).send(error.details[0].message);
    
    const flowInfo = new FlowInfo(flowInfoObject);
    const result = await flowInfo.save();

    return res.send(result);
};

const getFlowInfo = async (req, res, next) => {
    const flowInfo = await FlowInfo.findById(req.params.id);
    if (!flowInfo) res.status(400).send('Invalid flow id.');
    
    return res.send(flowInfo);
};

const getFlowInfos = async (req, res, next) => {
    const flowInfos = await FlowInfo.find();
    return res.send(flowInfos);
};

const updateFlowInfo = async (req, res, next) => {

};

const deleteFlowInfo = async (req, res, next) => {

};

module.exports = {
    createFlowInfo,
    getFlowInfo,
    getFlowInfos,
    updateFlowInfo,
    deleteFlowInfo
};