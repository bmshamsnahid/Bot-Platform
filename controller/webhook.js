const receivedAuthenticationEvent = require('../event/receivedAuthentication');
const receivedMessageEvent = require('../event/receivedMessage');
const receivedDeliveryConfirmationEvent = require('../event/receivedDeliveryConfirmation');
const receivedPostBackEvent = require('../event/receivedPostback');
const receivedMessageReadEvent = require('../event/receivedMessageRead');
const receivedAccountLinkEvent = require('../event/receivedAccountLink');

const config = require('config');
const { BotInfo }  = require('../model/bot/botInfo');

const validateToken = async (req, res, next) => {
    const botInfo = await BotInfo.findOne({ pageId: req.params.pageId });
    let webHookToken;
    console.log('BOT INFO');
    console.log(botInfo);
    if (botInfo) {
        webHookToken = botInfo.webHookToken;
    } else {
        webHookToken = (process.env.MESSENGER_VALIDATION_TOKEN) ? (process.env.MESSENGER_VALIDATION_TOKEN) : config.get('validationToken');
    }
    if (req.query['hub.mode'] === 'subscribe' &&
        req.query['hub.verify_token'] === webHookToken) {
        console.log("Validating webhook");
        return res.status(200).send(req.query['hub.challenge']);
    } else {
        console.log('Provided: ' + req.query['hub.verify_token']);
        console.error("Failed validation. Make sure the validation tokens match.");
        return res.sendStatus(403);
    }
};

const handleWebHookPostBack = (req, res, next) => {
    const data = req.body;
    const botId = req.params.botId;

    // Make sure this is a page subscription
    if (data.object == 'page') {
        // Iterate over each entry
        // There may be multiple if batched
        data.entry.forEach(function(pageEntry) {
            var pageID = pageEntry.id;
            var timeOfEvent = pageEntry.time;

            // Iterate over each messaging event
            pageEntry.messaging.forEach(function(messagingEvent) {
                if (messagingEvent.optin) {
                    receivedAuthenticationEvent(messagingEvent);
                } else if (messagingEvent.message) {
                    receivedMessageEvent(messagingEvent, botId);
                } else if (messagingEvent.delivery) {
                    receivedDeliveryConfirmationEvent(messagingEvent);
                } else if (messagingEvent.postback) {
                    receivedPostBackEvent(messagingEvent);
                } else if (messagingEvent.read) {
                    receivedMessageReadEvent(messagingEvent);
                } else if (messagingEvent.account_linking) {
                    receivedAccountLinkEvent(messagingEvent);
                } else {
                    console.log("Webhook received unknown messagingEvent: ", messagingEvent);
                }
            });
        });

        // Assume all went well.
        //
        // You must send back a 200, within 20 seconds, to let us know you've
        // successfully received the callback. Otherwise, the request will time out.
        res.sendStatus(200);
    }
};

module.exports = {
    handleWebHookPostBack,
    validateToken
};