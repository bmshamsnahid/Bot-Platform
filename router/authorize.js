const express = require('express');
const router = express.Router();
const accountLinking = require('../controller/accountLinking');

/*
 * This path is used for account linking. The account linking call-to-action
 * (sendAccountLinking) is pointed to this URL.
 *
 */
router.get('/', accountLinking.linkAccount);

module.exports = router;