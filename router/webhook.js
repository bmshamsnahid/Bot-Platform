const express = require('express');
const router = express.Router();

const webHookHandler = require('../controller/webhook');

/*
 * Use your own validation token. Check that the token used in the Webhook
 * setup is the same token used here.
 *
 */
router.get(['/', '/:pageId'], webHookHandler.validateToken);

/*
 * All callbacks for Messenger are POST-ed. They will be sent to the same
 * webhook. Be sure to subscribe your app to your page to receive callbacks
 * for your page.
 * https://developers.facebook.com/docs/messenger-platform/product-overview/setup#subscribe_app
 *
 */
router.post(['/', '/:pageId'], webHookHandler.handleWebHookPostBack);

module.exports = router;