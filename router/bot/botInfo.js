const express = require('express');
const router = express.Router();
const botInfoController = require('../../controller/bot/botInfo');

router.post('/', botInfoController.createBotInfo);
router.get('/', botInfoController.getAllBotInfo);
router.get('/:id', botInfoController.getBotInfo);
router.patch('/:id', botInfoController.updateBotInfo);
router.delete('/:id', botInfoController.deleteBotInfo);

module.exports = router;