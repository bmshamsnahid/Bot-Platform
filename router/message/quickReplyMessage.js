const express = require('express');
const router = express.Router();
const quickReplyMessageController = require('../../controller/message/quickReplyMessage');

router.post('/', quickReplyMessageController.createQuickReplyMessage);
router.get('/:id', quickReplyMessageController.getQuickReplyMessage);
router.get('/', quickReplyMessageController.getQuickReplyMessages);
router.patch('/:id', quickReplyMessageController.updateQuickReplyMessage);
router.delete('/:id', quickReplyMessageController.deleteQuickReplyMessage);

module.exports = router;