const express = require('express');
const router = express.Router();
const textMessageController = require('../../controller/message/text');

router.post('/', textMessageController.createMessage);

module.exports = router;