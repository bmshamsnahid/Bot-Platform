const { QuickReplyInfo, validateQuickReplyInfo, assignPayload } = require('../../model/quickReply/quickReplyInfo');
const _ = require('lodash');

const createQuickReplyInfo = async (quickReply) => {
    const { error } = validateQuickReplyInfo(quickReply);
    if (error) return { error : error };

    const quickReplyInfo = new QuickReplyInfo(quickReply);
    const result = await quickReplyInfo.save();
    
    return result;
};

const getQuickReplyInfos = async () => {
    const quickReplyInfos = await QuickReplyInfo.find();
    res.send(quickReplyInfos);
};

const getQuickReplyInfo = async (id) => {
    const quickReplyInfo = await QuickReplyInfo.findById(id);
    res.send(quickReplyInfo);
};

const updateQuickReplyInfo = async () => {

};

const deleteQuickReplyInfo = async () => {

};

const createMultipleQuickReplies = async (quickReplies) => {
    const validatedQuickReplies = [];
    const quickRepliesId = [];
    
    // validate all the quick replies
    for (let index=0; index<quickReplies.length; index++) {
        const currentQuickReply = assignPayload(quickReplies[index]);
        const { error } = validateQuickReplyInfo(currentQuickReply);
        if (error) {
            return {
                error: error,
                quickRepliesId: null
            }
        } else {
            const model = new QuickReplyInfo(currentQuickReply);
            validatedQuickReplies.push(model);
        }
    }

    // saving the quick replies
    for (let index=0; index<validatedQuickReplies.length; index++) {
        const currentValidatedQuickReply = validatedQuickReplies[index];
        await currentValidatedQuickReply.save();
        quickRepliesId.push(currentValidatedQuickReply._id);
    }

    return {
        error: null,
        quickRepliesId: quickRepliesId
    }
};

module.exports = {
    createQuickReplyInfo,
    getQuickReplyInfos,
    getQuickReplyInfo,
    updateQuickReplyInfo,
    deleteQuickReplyInfo,
    createMultipleQuickReplies
};